"""
Файл с классом-конфигурацией приложения
"""
import os


class Config:
    """
    Класс, необходимый для настройки конфигурации приложения
    ...

    Методы:
        debug_on()
            Включить режим отладки
        csrf_off()
            Выключить проверки csrf
    Поля:
    REPOSITORY_PATH - Путь на ресурсе Gitlab до каталога results
    HOST_ADDRESS - ip, на котором будет запущен микросервис
    HOST_PORT - порт, на котором будет запущен микросервис
    """

    HOST_REPOSITORY = os.path.join('/kidp-dl', 'app', 'tmp')
    SECRET_KEY = 'fdgfh78@#5?>gfhf89dx,v06k'
    GITLAB_URL = 'http://gitlab.com'
    REPOSITORY_PATH = 'reposwithrepos'
    HOST_ADDRESS = '0.0.0.0'
    HOST_PORT = 5000
    FOLDER_WITH_REPOS = 'results'
    FOLDER_WITH_CODE_REPOS = 'dev'
    FOLDER_WITH_DOCK_REPOS = 'work'

    def __init__(self):
        self.__debug = False
        self.__csrf_enabled = True

    def debug_on(self):
        """
        Включить режим отладки
        """
        self.__debug = True

    def csrf_off(self):
        """
        Выключить проверки csrf
        """
        self.__csrf_enabled = False
