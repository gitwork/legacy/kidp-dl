ARG VERSION
FROM kidp-dl-base:${VERSION}

COPY ./app ./kidp-dl/app
COPY ./config.py ./kidp-dl
COPY ./manage.py ./kidp-dl

RUN mkdir /kidp-dl/app/tmp/repos

WORKDIR /kidp-dl

ENTRYPOINT ["python3", "manage.py"]
