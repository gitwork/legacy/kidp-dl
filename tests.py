"""File with tests"""
import os
import unittest

from coverage import coverage
from prettytable import PrettyTable

from app.scripts.file_parser import FileParser
from app.scripts.gitlab_connection import Connection
from app.scripts.kidp_results import PrintRepository
from app.scripts.valid_values import ValidValues
from config import Config
#from app.scripts.kidp_results import
from app.scripts.listener_data import ListenerData
from app.scripts.repository import Repository, MergeRequestRepository
from app.scripts.result import Result, RepositoryResult

REPOS_NAME = 'sir.med2012/mytestrepository'
TOKEN = 'JyeixmYyeMZTHsjZjhTf'
ERROR_TOKEN = 'JyeixmYyeMZTHsWEVWV'
WORK = 'диплом'
URL = Config.GITLAB_URL
CONNECTION = Connection(TOKEN)
REPOSITORY = Repository(REPOS_NAME, CONNECTION.get_connection())


class TestConnection(unittest.TestCase):
    def setUp(self):
        self.connection = Connection(TOKEN)

    def test_connect(self):
        self.assertIsNotNone(CONNECTION.get_connection())

    def error_connect(self):
        error_connection = Connection(ERROR_TOKEN)
        self.assertEqual(False, error_connection.get_connection())


class TestRepository(unittest.TestCase):

    def test_get_repository(self):
        for i in REPOSITORY.get_id_list():
            self.assertIsNotNone(REPOSITORY.get_repository(i.id))

    def test_bad_repos_name(self):
        repos_name = 'WEGUVAYGH/'
        repository = Repository(repos_name, CONNECTION.get_connection())
        self.assertEqual(repository.get_id_list(), [])


class TestKidpResults(unittest.TestCase):
    def setUp(self):
        self.data1 = {'full_name': '2018-4-11-med', 'flag': False, 'readme': True, 'note_words': 0,
                         'project': True, 'paper': False, 'presentation': False, 'text': True,
                         'note': True, 'closed_issue': 12, 'all_issue': 24, 'not_load': True,
                         'closed_issue_ms': 3, 'all_issue_ms': 9, 'commits': 21, 'size': 123422}
        self.data2 = {'full_name': '2018-4-12-med', 'flag': True, 'readme': False, 'note_words': 0,
                      'project': False, 'paper': 0, 'presentation': 12, 'text': 34, 'note': 12,
                      'closed_issue': 0, 'all_issue': 0, 'closed_issue_ms': 0, 'not_load': False,
                      'all_issue_ms': 9, 'commits': 21, 'size': 1234222}

        self.table = PrettyTable(['FULLNAME','README','PROJECT','PAPER', 'RESULT(PPTX)','RESULT(DOCX)','NOTE',
                            'CLOSED/ALL ISSUE','PROGRESS','CLOSED/ALL MSTONE', 'PROGRESS MSTONE',
                            'COMMITCOUNT','REPOSSIZE'])

        self.table.add_row(['2018-4-11-med', '+', '+', '-', '-', '+', '+', '12/24', '50%', '3/9', '33%', 21, '0.12 MB'])
        self.table.add_row(['2018-4-12-med', '-', '-', 0, 12, 34, 12, '0/0', '0%', '0/9', '0%', 21, '1.18 MB'])
        self.result = [self.data1, self.data2]

    def test_web_table(self):
        k_result = PrintRepository(self.result, {})
        self.assertEqual(k_result.web_table(),
            [['2018-4-11-med', 33, '3/9', 'do not load/+', '-', '+', '-', '12/24', 50, '+', '+', 21,0.12],
             ['2018-4-12-med', 0, '0/9', 12, 12, 34, '-', '0/0', 0, '-', '-', 21, 1.18]])


class TestListenerData(unittest.TestCase):
    def setUp(self):
        self.repos = MergeRequestRepository('https://gitlab.com/sir.med2012/mytestrepository',
                                            CONNECTION.get_connection()).get_repository(25677280)
        self.l_data = ListenerData(self.repos, WORK)

    def test_listener_data(self):
        self.assertEqual(self.l_data.get_name(), self.repos.name)
        self.assertEqual(self.l_data.get_repository_data(), self.repos)
        self.l_data.get_data()
        self.assertEqual(self.l_data.get_result(), { 'all_issue': 0,
                                                     'all_issue_ms': 0,
                                                     'closed_issue': 0,
                                                     'closed_issue_ms': 0,
                                                     'commits': 9,
                                                     'full_name': 'MyTestRepository',
                                                     'size': 576716})


class Repos:
    name = 'name'


class TestFileParser(unittest.TestCase):
    def setUp(self):
        self.flag1 = 'short'
        self.flag2 = 'full'
        self.link = ''
        self.repos = Repos
        self.short_result = {'flag': False, 'link': '', 'not_load': False, 'note_words': 0,
                             'project': True, 'paper': False,
                             'readme': True, 'presentation': True, 'note': True, 'text': True}
        self.full_result = {'flag': True, 'link': '',
                            'not_load': False, 'note': '2', 'note_words': 0, 'paper': 0,
                            'presentation': 3, 'project': True, 'readme': True, 'text': '1'}

    def test_short_data(self):
        f_parser = FileParser(self.repos, WORK, None, False, os.path.abspath('app/test/test_data'))
        self.assertEqual(f_parser.get_file_data(), self.short_result)

    def test_full_data(self):
        f_parser = FileParser(self.repos, WORK, self.flag2, False,  os.path.abspath('app/test/test_data'))
        self.assertEqual(f_parser.get_file_data(), self.full_result)
        self.assertEqual(f_parser._has_link(), self.link)


class TestValidValues(unittest.TestCase):
    def setUp(self):
        self.session = {'reposName': '2020-2021/kr-4k', 'inputToken': TOKEN, 'radio': 'kr-4k'}
        self.session1 = {'name-1': '', 'inputToken': TOKEN}
        self.equal2 = {}
        self.equal3 = {}

    def test_check_repository(self):
        self.assertEqual(False, ValidValues(self.session).check_repository())

    def test_check_mr(self):
        self.assertEqual(False, ValidValues(self.session1).check_mr())

    def test_check_list(self):
        self.assertEqual([], ValidValues(self.session).check_list())


if __name__ == '__main__':
    unittest.main()
