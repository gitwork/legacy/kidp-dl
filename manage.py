""" Файл с запуском приложения """
from app import app
from config import Config

if __name__ == '__main__':
    app.run(host=Config.HOST_ADDRESS, port=Config.HOST_PORT)
