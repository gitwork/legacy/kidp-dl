"""
Файл с классом, который необходим для соединения с ресурсом Gitlab и получением объекта Gitlab
"""
import gitlab
from flask import app
from config import Config
import urllib3
import requests

urllib3.disable_warnings()
requests.packages.urllib3.disable_warnings()


class Connection:
    """
    Класс для представления соединения с ресурсом Gitlab
    ...

    Атрибуты
    token : str
        Приватный токен Gitlab

    Методы
    get_connection()
        Получить соединение с ресурсом Gitlab
    check_connection()
        Проверить корректность соединения с ресурсом Gitlab через приватный токен
    """
    __gitlab_url = Config.GITLAB_URL

    def __init__(self, token):
        """
        Устанавливает необходимый атрибут для объекта connection

        param:
        token : str
            Приватный токен Gitlab
        """
        self.__token = token

    def get_connection(self):
        """
        Получает соединение с ресурсом Gitlab и возвращает его

        :return
            Соединение с ресурсом Gitlab при корректно введенном токене
            Сообщение об ошибке в противном случае
        """
        return self.__connect() if self.check_connection() else self.__error_connection()

    def check_connection(self):
        """
        Проверяет корректность введенного приватного токена

        :return
            True при корректном токене
            False в противном случае
        """
        result = False
        try:
            result = self.__connect().auth() is None
        except gitlab.exceptions.GitlabAuthenticationError:
            app.logging.error('Authorization error in Gitlab. Bad Token')
        return result

    def __connect(self):
        return gitlab.Gitlab(self.__gitlab_url, self.__token, ssl_verify=False)

    def __error_connection(self):
        return f"Connection from {self.__gitlab_url} failed"

    def get_token(self):
        return self.__token
