"""
Файл с классом, который анализирует правильность введенных данных
"""
import re
from app.scripts.gitlab_connection import Connection
from config import Config


class ValidValues:
    """
    Класс для проверки правильности введенных данных на веб-странице
    ...

    Атрибуты
    values : dict
        Словарь значений, полученных с веб-страницы

    Методы:
    check_repository()
        Проверяет правильность введенных данных на странице /main
    check_mr()
        Проверяет правильность введенных данных на странице /stats
    check_list()
        Проверка правильности введенного списка репозиториев
    """

    def __init__(self, values):
        """
        Устанавливает необходимый атрибут для объекта valid_values

        :param
        values : dict
            Словарь значений, полученных с веб-страницы
        """
        self.__session = values

    def check_repository(self):
        """
        Проверяет правильность введенных данных на странице /main

        :return
        True, если все верно
        False, если перепутаны флаги или данные некорректны
        """
        result_repos = False
        if len(self.__session['reposName']) != 0 and '/' in self.__session['reposName']:
            if self.__session['reposName'].split('/')[0] == 'kr-4k' and \
                    (self.__session['radio'] == 'kurs2' or self.__session['radio'] == 'kurs1'):
                result_repos = True
            elif self.__session['reposName'].split('/')[0] == 'dipl' \
                    and self.__session['radio'] == 'diplom':
                result_repos = True
            elif self.__session['reposName'].split('/')[0] == 'practike-3k' \
                    and self.__session['radio'] == 'prac':
                result_repos = True
        if self.__session['radioH'] == 'net':
            return result_repos and Connection(self.__session['inputToken']).check_connection()
        else:
            return result_repos

    def check_mr(self):
        """
        Проверяет правильность введенных данных на странице /stats

        :return
        True, если все верно
        False, усли перепутаны флаги или данные некорректны
        """
        try:
            result = len(self.__session['inputToken'])
        except KeyError:
            result = False
        return result and Connection(self.__session['inputToken']).check_connection()

    def check_list(self):
        """
        Проверяет правильность введенных данных в формы списка ссылок

        :return
        Форматированный список ссылок
        """
        repos_list = []
        for i in self.__session:
            if re.findall(r'name', i) and self.__session[i].find(Config.GITLAB_URL) == 0:
                repos_list.append(self.__session[i])
        return repos_list
