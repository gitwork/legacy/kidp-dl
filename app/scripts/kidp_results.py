"""
Файл с классами-отображениями таблиц и обработанных данных
"""
import csv
import os
from abc import ABC, abstractmethod
from config import Config


class PrintResult(ABC):
    """
    Родительский класс

    """
    _table_header = []
    _table_data = []
    _table_list = []
    _branch_list = []

    def __init__(self, result, log_result, net_or_host, repos_name):
        self.__result = result
        self.__log_result = log_result
        self._net_or_host = net_or_host
        self._repos_name = repos_name

    def get_header(self):
        """
        Возвращает заголовок таблицы

        :return
        Список со названиями колонок таблицы
        """
        return self._table_header

    def clear(self):
        """clearing some stuff"""
        self._table_header.clear()
        self._table_data.clear()
        self._table_list.clear()
        self._branch_list.clear()

    def web_table(self):
        """
        Формирует данные для отображения на веб-странице

        :return
        Список списков. В каждом внутреннем списке - обработанные данные
        """
        self._table_list.clear()
        self._branch_list.clear()
        for data_dict in self.__result:
            if data_dict and self._net_or_host == 'net':
                res = self._make_row(data_dict)
                result = res[0]
                result = result.copy()
                branch_dev = res[1]
                branch_work = res[2]
                self._branch_list.append([branch_dev, branch_work])
                self._table_list.append(result)
            elif data_dict and self._net_or_host == 'host':
                result = self._make_row(data_dict)
                result = result.copy()
                self._table_list.append(result)
        self._file_table()
        if self._net_or_host == 'net':
            return [self._table_list, self._branch_list]
        return self._table_list

    def log_table(self):
        """
        Формирует данные ошибок обработки файлов для отображения на веб-странице

        :return
        Список списков. В каждом внутреннем списке - обработанные данные
        """
        for data_dict in self.__log_result:
            if data_dict:
                result = self._make_row(data_dict)[0]
                result = result.copy()
                self._table_list.append(result)
        return self._table_list

    def _file_table(self):
        """
        Записывает обработанные данные в файл формата csv

        :return
        Файл result.csv в папке tmp
        """
        file_name = os.path.join('.', 'app', 'tmp', 'result.csv')
        with open(file_name, mode="w+", encoding='utf-8') as w_file:
            file_writer = csv.writer(w_file, delimiter=";", lineterminator="\r")
            file_writer.writerow(self._make_header())
            for data_dict in self.__result:
                if data_dict:
                    file_writer.writerow(self._make_row(data_dict)[0])

    def make_log(self):
        """
        Формирует данные для отображения на веб-странице

        :return
        Список списков. В каждом внутреннем списке - обработанные данные
        """
        result = []
        for i in self.__log_result:
            if i is not None:
                for j in i:
                    for value in self.__log_result[self.__log_result.index(i)].get(j):
                        result_str = [j, value]
                        result.append(result_str)
        return result

    @abstractmethod
    def _make_header(self):
        pass

    @abstractmethod
    def _make_row(self, data_dict):
        pass


class PrintRepository(PrintResult):
    """
    Класс, необходимый для создания таблицы по КИДП

    Методы:
        make_header()
            Заполняет шапку результирующей таблицы по Merge Request
        make_row()
            Заполняет список данными из словаря
    """

    def _make_header(self):
        """
        Заполняет шапку результирующей таблицы по Merge Request

        :return
        Список с колонками таблицы
        """
        self._table_header.clear()
        self._table_header.append('FULLNAME')
        if self._net_or_host == 'net':
            self._table_header.append('ВЕТОК dev')
            self._table_header.append('ВЕТОК work')
        self._table_header.append('ПРЕЗЕНТАЦИЯ')
        self._table_header.append('ПЗ')
        self._table_header.append('ПРИМЕЧ')
        self._table_header.append('ДОКЛАД')
        self._table_header.append('СТАТЬЯ')
        self._table_header.append('README dev')
        self._table_header.append('README work')
        self._table_header.append('PROJECT')
        self._table_header.append('КОММИТОВ dev')
        self._table_header.append('КОММИТОВ work')

        if self._net_or_host == 'net':
            self._table_header.append('ПЛАН-ГРАФИК(%)')
            self._table_header.append('ЗАДАЧИ В ПГ')
            self._table_header.append('ЗАДАЧИ ВНЕ ПГ')
            self._table_header.append('ПРОГРЕСС(%)')
            self._table_header.append('РАЗМЕР(MB)')
        return self._table_header

    def _make_row(self, data_dict):
        """
        Заполняет список данными из словаря

        :return
        Список - строка таблицы
        """
        if self._net_or_host == 'net':
            branches_dev = [data_dict['branches']['dev'],
                            os.path.join(f"{Config.GITLAB_URL}", f"{Config.FOLDER_WITH_REPOS}",
                                         f"{self._repos_name}",
                                         f"{Config.FOLDER_WITH_CODE_REPOS}",
                                         f"{data_dict['full_name']}", "-", "branches")]
            branches_work = [data_dict['branches']['work'],
                             os.path.join(f"{Config.GITLAB_URL}", f"{Config.FOLDER_WITH_REPOS}",
                                          f"{self._repos_name}",
                                          f"{Config.FOLDER_WITH_DOCK_REPOS}",
                                          f"{data_dict['full_name']}", "-", "branches")]
        self._table_data.clear()
        s_format = StringFormat(data_dict)
        self._table_data.append(data_dict['full_name'])
        if self._net_or_host == 'net':
            self._table_data.append(data_dict['branches']['dev'])
            self._table_data.append(data_dict['branches']['work'])
        self._table_data.append(s_format.set_pptx())
        self._table_data.append(s_format.set_note())
        self._table_data.append(s_format.set_note_words())
        self._table_data.append(s_format.set_docx())
        self._table_data.append(s_format.set_paper())
        self._table_data.append('+') if data_dict['readme']['dev'] else self._table_data.append('-')
        self._table_data.append('+') if data_dict['readme']['work'] else self._table_data.append('+')
        self._table_data.append('+' if data_dict['project'] else '-')
        self._table_data.append(str(data_dict['commits']['dev']).replace('\n', ''))
        self._table_data.append(str(data_dict['commits']['work']).replace('\n', ''))

        if self._net_or_host == 'net':
            self._table_data.append(s_format.set_progress_milestone())
            self._table_data.append(os.path.join(f"{data_dict['closed_issue_ms']}", f"{data_dict['all_issue_ms']}"))
            self._table_data.append(os.path.join(f"{data_dict['closed_issue']}", f"{data_dict['all_issue']}"))
            self._table_data.append(s_format.set_progress())
            self._table_data.append(round(data_dict['size'] / (1024 * 1024), 2))
            return [self._table_data, branches_dev, branches_work]
        return self._table_data


class PrintMergeRequest(PrintResult):
    """
    Класс, необходимый для создания таблицы по Merge Request

    Методы:
        make_header()
            Заполняет шапку результирующей таблицы по Merge Request
        make_row()
            Заполняет список данными из словаря
    """

    def _make_header(self):
        """
        Заполняет шапку результирующей таблицы по Merge Request

        :return
        Список с колонками таблицы
        """
        self._table_header.clear()
        self._table_header.append('ПРОЕКТ')
        self._table_header.append('FULLNAME')
        self._table_header.append('НАЗВАНИЕ MR')
        self._table_header.append('MR_UNRESOLVED')
        self._table_header.append('MR_OVERVIEWS')
        self._table_header.append('КОММИТОВ')
        self._table_header.append('ДАТА ПРОСМОТРА')
        return self._table_header

    def _make_row(self, data_dict):
        """
        Заполняет список данными из словаря

        :return
        Список - строка таблицы
        """
        self._table_data.clear()
        self._table_data.append(data_dict['project'])
        self._table_data.append(data_dict['full_name'])
        self._table_data.append(data_dict['mr_name'])
        self._table_data.append(data_dict['unresolved'])
        self._table_data.append(data_dict['overviews'])
        self._table_data.append(data_dict['commits'])
        self._table_data.append(data_dict['data'])
        return self._table_data


class StringFormat:
    """
    Класс для форматирования данных из словаря в строку
    ...

    Атрибуты:
    data_dict : dict
        Словарь с данными для форматирования
    """

    def __init__(self, data_dict):
        """
        Устанавливает необходимый атрибут для объекта string_format

        :param
        data_dict : dict
            Словарь с данными для форматирования
        """
        self.__data_dict = data_dict

    def set_paper(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        if self.__data_dict['flag']:
            result = '-' if self.__data_dict['paper'] == 0 else self.__data_dict['paper']
        else:
            result = '+' if self.__data_dict['paper'] else '-'
        return result

    def set_pptx(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        if self.__data_dict['flag']:
            result = '-' if self.__data_dict['presentation'] == 0 \
                else self.__data_dict['presentation']
        else:
            result = '+' if self.__data_dict['presentation'] else '-'
        return result

    def set_docx(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        if self.__data_dict['flag']:
            result = '-' if self.__data_dict['text'] == 0 else self.__data_dict['text']
        else:
            result = '+' if self.__data_dict['text'] else '-'
        return result

    def set_note(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        if self.__data_dict['flag']:
            result = '-' if self.__data_dict['note'] == 0 else self.__data_dict['note']
        else:
            result = '+' if self.__data_dict['note'] else '-'
        return result

    def set_note_words(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        if self.__data_dict['flag']:
            result = '-' if self.__data_dict['note_words'] == 0 else self.__data_dict['note_words']
        else:
            result = '+' if self.__data_dict['note_words'] else '-'
        return result

    def set_progress(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        return int(self.__data_dict['closed_issue'] * 100 / self.__data_dict['all_issue']) \
            if self.__data_dict['all_issue'] != 0 else 0

    def set_progress_milestone(self):
        """
        Форматирующий метод

        :return
            Отформатированная строка
        """
        return int(self.__data_dict['closed_issue_ms'] * 100 / self.__data_dict['all_issue_ms']) \
            if self.__data_dict['all_issue_ms'] != 0 else 0
