"""
Файл с классами, которые собирают все данные и возвращают одну структуру данных
для отображения на веб-странице
"""
import os
import re
from abc import ABC, abstractmethod
from app.scripts.file_parser import FileParser, NetworkParser, has_readme_host
from app.scripts.gitlab_connection import Connection
from app.scripts.listener_data import ListenerData, MergeRequestData
from app.scripts.repository import Repository, MergeRequestRepository
from config import Config


class Result(ABC):
    """ Class mediator between net and host """
    _result = []

    def __init__(self, repos_name, token):
        self._repos_name = repos_name
        self._token = token

    @abstractmethod
    def get_stats(self):
        """
        Абстрактный метод
        """

    def get_result(self):
        """
        Возвращает список для веб-представления
        :return:
        Список с данными
        """
        return self._result


class RepositoryResult(Result):
    """
    Расширение родительского класса для работы с КИДП
    """
    __res_list = []
    __dev_list = []
    __dev_data = None
    _log_result = []

    def __init__(self, repos_name, token, work, full, result_dir, repos_type):
        super().__init__(repos_name, token)
        self.__full = full
        self.__result_dir = Config.HOST_REPOSITORY if result_dir == 'host' else 'net'
        self.__work = work
        self.__repos_type = repos_type

    def _get_stats_from_dev(self, dev_repo, repository, gl_connect):
        self.__dev_data = repository.get_repository(dev_repo)
        self.__dev_list.remove(dev_repo)
        is_there_dev = True
        repos_dev = repository.get_repository(dev_repo)
        l_data_dev = ListenerData(repos_dev, self.__work)
        f_data_dev = NetworkParser(l_data_dev.get_repository_data(),
                                   self.__work, self.__full,
                                   self.__repos_type, gl_connect,
                                   self.__dev_data)
        dev_readme = True if f_data_dev.has_readme() else False
        commits_in_dev = f_data_dev.get_commit_count()
        branches_in_dev = f_data_dev.get_branches()
        return {'dev_readme': dev_readme, 'commits_in_dev': commits_in_dev,
                'branches_in_dev': branches_in_dev, 'is_there_dev': is_there_dev}

    def _get_stats_for_each_slush(self, repository, repos_id, gl_connect):
        repos = repository.get_repository(repos_id)
        l_data = ListenerData(repos, self.__work)
        dev_stats = {'dev_readme': False, 'commits_in_dev': 0,
                     'branches_in_dev': 0, 'is_there_dev': False}
        for dev in self.__dev_list:
            if re.search(Config.FOLDER_WITH_CODE_REPOS, repository.get_repository(dev).web_url):
                dev_stats = self._get_stats_from_dev(dev, repository, gl_connect)

        f_data = NetworkParser(l_data.get_repository_data(),
                               self.__work, self.__full,
                               self.__repos_type, gl_connect,
                               self.__dev_data)
        work_readme = True if f_data.has_readme() else False
        file_result = f_data.get_file_data(self.__work)
        f_data.set_readme_net_work(work_readme)
        if dev_stats['is_there_dev']:
            f_data.set_readme_net_dev(dev_stats['dev_readme'])
        log_list = f_data.get_log_list()
        if log_list:
            self._log_result.append(f_data.get_log_list())
        repos_full_name = l_data.get_result()['full_name']
        if file_result['link']:
            mr_repos = MergeRequestRepository(file_result['link']
                                              .split(f"{Config.GITLAB_URL}/")[1],
                                              gl_connect.get_connection())
            l_data = ListenerData(mr_repos.get_repository(mr_repos.get_id_list()), self.__work)
        l_data.get_result()['full_name'] = repos_full_name
        l_data.get_result().update(file_result)
        commit = {'dev': dev_stats['commits_in_dev'], 'work': f_data.get_commit_count()}
        branch = {'dev': dev_stats['branches_in_dev'], 'work': f_data.get_branches()}
        data_dict = {'commits': commit, 'branches': branch}
        data_dict.update(l_data.get_result())
        self._result.append(data_dict)

    def get_stats(self):
        """
        Основной метод всего микросервиса
        Вся логика работы программы здесь
        :return
        Список со словарями обработанных данных
        """
        self._result.clear()
        self.__res_list.clear()
        self.__dev_list.clear()
        self.__dev_data = None
        self._log_result.clear()

        gl_connect = Connection(self._token)
        repository = Repository(self._repos_name, gl_connect.get_connection())
        for repos_id in self.__get_list(repository.get_id_list(), repository):
            self._get_stats_for_each_slush(repository, repos_id, gl_connect)
        return self._result

    def get_stats_host(self):
        """
        Основной метод всего микросервиса
        Вся логика работы программы здесь
        :return
        Список со словарями обработанных данных
        """
        self._result.clear()
        self.__res_list.clear()
        self.__dev_list.clear()
        self.__dev_data = None
        self._log_result.clear()

        for slush in os.listdir(
                os.path.join(f"{self.__result_dir}", f"{Config.FOLDER_WITH_REPOS}", f"{self._repos_name}",
                             f"{Config.FOLDER_WITH_DOCK_REPOS}")):
            f_data = FileParser(f"{slush}", self.__work, self.__full, self.__repos_type,
                                os.path.join(f"{self.__result_dir}", f"{Config.FOLDER_WITH_REPOS}",
                                             f"{self._repos_name}", f"{Config.FOLDER_WITH_DOCK_REPOS}", f"{slush}"))
            file_result = f_data.get_file_data(self.__work)
            file_result['readme'] = has_readme_host(
                os.path.join(f"{self.__result_dir}", f"{Config.FOLDER_WITH_REPOS}", f"{self._repos_name}"), slush)
            log_list = f_data.get_log_list()
            if log_list:
                self._log_result.append(f_data.get_log_list())

            data_dict = {'full_name': f"{slush}",
                         'closed_issue': 0,
                         'all_issue': 0,
                         'closed_issue_ms': 0,
                         'all_issue_ms': 0,
                         'commits': f_data.commits_host(slush),
                         'size': 0
                         }
            data_dict.update(file_result)
            self._result.append(data_dict)
        return self._result

    def get_log_list(self):
        """
        :return:
        Список с ошибками для веб-представления
        """
        return self._log_result

    def __get_list(self, id_list, repos):
        self.__res_list.clear()
        for i in id_list:
            repos_data = repos.get_repository(i)
            if not re.search(Config.FOLDER_WITH_CODE_REPOS, repos_data.web_url):
                self.__res_list.append(repos_data.id)
            else:
                self.__dev_list.append(repos_data.id)
        return self.__res_list

    @staticmethod
    def __get_link_list(id_list):
        result_list = []
        for i in id_list:
            result_list.append(i.id)
        return result_list


class MergeRequestResult(Result):
    """
    Реализация базового класса для статистики по Merge Request
    """

    def get_stats(self):
        """
        Основной метод всего микросервиса
        Вся логика работы программы здесь

        :return
        Список со словарями обработанных данных
        """
        self._result.clear()
        gl_connect = Connection(self._token)
        repos_list = []
        for i in self._repos_name:
            repos_list.append(i.split(f"{Config.GITLAB_URL}/")[1])
        for repos_name in repos_list:
            repos = MergeRequestRepository(repos_name, gl_connect.get_connection())
            id_list = [repos.get_id_list()]
            for i in id_list:
                m_data = MergeRequestData(repos.get_repository(i))
                for k in m_data.get_stats():
                    self._result.append(k)
        return self._result

    def get_result(self):
        """
        Метод, повторяющий метод из базового класса
        """
        return self._result
