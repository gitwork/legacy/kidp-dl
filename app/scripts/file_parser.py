""" Файл с классами-обработчиками файлов, находящихся в репозитории """
import io
import logging
import os
import re
import zipfile
from abc import abstractmethod, ABC
import gitlab
from sh import git, ls, cd
from lxml import etree
from config import Config


class Parser(ABC):
    """
    Родительский класс
    ...

    Методы
    get_file_data()
        Собирает данные с репозитория
    get_log_list()
        Собирает логи ошибок обработки репозитория
    """

    def __init__(self, repository_data, type_of_work, short_type, repos_type):
        """
        Устанавливает необходимые атрибуты для объекта parser

        :param
        repository_data : gitlab.project.Gitlab
        :param
        name : str
        :param
        work : bool
        :param
        full : bool
        :param
        repos_type : bool
        """
        self._result = {}
        self._project_tree = []
        self._result_log_list = []
        self._result_log_dict = {}
        self._name = None
        self._repository_data = repository_data
        # вместо text.docx ожидается text2.docx в этом случае работы 2 семестра
        self._is_full = bool(short_type == 'full')
        self._repos_type = bool(repos_type == 'new')

        self._result_docx = 'result2.docx' if type_of_work == 'kurs2' else 'result.docx'
        self._text = "text2.docx" if type_of_work == 'kurs2' else "text.docx"
        self._result_pptx = "result2.pptx" if type_of_work == 'kurs2' else "result.pptx"
        self._paper = "paper.docx"

    def get_file_data(self, type_of_work):
        """
        Собирает данные с репозитория

        :return
            Словарь с данными
        """
        try:
            self._get_project_tree()
            self._result['flag'] = self._is_full
            self._result['project'] = self._has_project()
            self._result['readme'] = {'dev': False, 'work': False}
            self._result['paper'] = self._count_paper()
            self._result['presentation'] = self._count_slides()
            self._result['note_words'] = self._count_note_words()
            self._result['note'] = self._count_note()
            self._result['not_load'] = self._not_load()
            self._result['text'] = self._count_text(type_of_work)
            self._result['link'] = self._has_link()
            self._result_log_dict[self._name] = self._result_log_list
        except gitlab.GitlabGetError as err:
            self._get_project_tree()
            self._result['flag'] = False
            self._result['project'] = None
            self._result['readme'] = {'dev': False, 'work': False}
            self._result['paper'] = None
            self._result['presentation'] = None
            self._result['note'] = None
            self._result['note_words'] = 0
            self._result['not_load'] = None
            self._result['text'] = None
            self._result['link'] = None
            logging.error(f"Cant get some files for {self._repository_data.name} : {err}")
        return self._result

    def get_log_list(self):
        """
        Собирает данные ошибок обработки данных в репозитории

        :return
            Словарь с данными
        """
        return self._result_log_dict

    @abstractmethod
    def _has_link(self):
        pass

    @abstractmethod
    def _not_load(self):
        pass

    @abstractmethod
    def _count_slides(self):
        pass

    @abstractmethod
    def _count_text(self, type_of_work):
        pass

    @abstractmethod
    def _count_note(self):
        pass

    @abstractmethod
    def _count_note_words(self):
        pass

    @abstractmethod
    def _count_paper(self):
        pass

    @abstractmethod
    def _get_project_tree(self):
        pass

    @abstractmethod
    def _has_project(self):
        pass

    def has_readme(self):
        """Наличие README.md при удаленном анализе"""
        return 'README.md' in self._project_tree


class NetworkParser(Parser):
    """
    Расширение родительского класса для работы с файлами, находящихся на ресурсе Gitlab

    Атрибуты:
    gl_connect : gitlab.Object
        Соединение с ресурсом Gitlab
    dev_data : list
        Структура репозитория
    """

    def __init__(self, repository_data, type_of_work, short_type, repos_type, gl_connect, dev_data):
        """
        Устанавливает необходимые атрибуты для объекта parser

        :param
        gl_connect : gitlab.Object
        :param
        dev_data : list
        """

        super().__init__(repository_data, type_of_work, short_type, repos_type)
        self._gl_connect = gl_connect
        self._dev_data = dev_data
        self._get_project_tree()
        self._name = self._repository_data.name

    def get_commit_count(self):
        """Возвращает число коммитов в удаленном репо"""
        return len(self._repository_data.commits.list(all=True))

    def get_branches(self):
        return len(self._repository_data.branches.list())

    def set_readme_net_dev(self, dev_readme):
        self._result['readme']['dev'] = dev_readme

    def set_readme_net_work(self, work_readme):
        self._result['readme']['work'] = work_readme

    def _has_link(self):
        if 'link.txt' in self._project_tree:
            return bytes.decode(self._repository_data.files.get(file_path='link.txt', ref='master').decode(),
                                encoding='utf-8')
        else:
            return False

    def _has_project(self):
        if self._repos_type:
            result = bool(self._dev_data.repository_tree())
        else:
            result = 'project' in self._project_tree
        return result

    def _not_load(self):
        return 'do-not-load.txt' in self._project_tree

    def _count_slides(self):
        result = self._result_pptx
        if not self._is_full:
            return result in self._project_tree
        if result not in self._project_tree:
            return 0

        count = 0
        try:
            file_path = self._result_pptx
            presentation_file = self._repository_data.files.get(file_path=file_path,
                                                                ref='master')
            count = get_slides(self, presentation_file)
        except zipfile.BadZipFile:
            self._result_log_list.append('Невозможно прочитать данные из презентации.'
                                         ' Ошибка доступа к файлу')
            logging.error('Failed to get result.pptx in %s', self._name)
        except gitlab.GitlabGetError:
            self._result_log_list.append('Невозможно получить презентацию с ресурса Gitlab')
            logging.error('Failed to get result.pptx from Gitlab in %s', self._name)

        # реальных слайдов выдает на 2 больше, если презентация не пуста, поэтому уменьшаем на 2
        if count != 0:
            count -= 2
        return count

    def _count_paper(self):
        result = 0
        if self._paper in self._project_tree:
            try:
                paper_file = self._repository_data.files.get(file_path=self._paper, ref='master')
                result = get_metadata(self, paper_file)
            except zipfile.BadZipFile:
                self._result_log_list.append('Невозможно прочитать данные из статьи'
                                             ' Ошибка доступа к файлу')
                logging.error('Failed to get paper in %s', self._name)
            except gitlab.GitlabGetError:
                self._result_log_list.append('Невозможно получить файл статьи с ресурса Gitlab')
                logging.error('Failed to get paper from Gitlab in %s', self._name)
        return self._paper in self._project_tree if not self._is_full else result

    def _count_note_words(self):
        result = self._text
        comments = []
        if result in self._project_tree:
            try:
                ooXMLns = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}
                docxZip = zipfile.ZipFile(self._repository_data.files.get(file_path=result, ref='master'))
                commentsXML = docxZip.read('word/comments.xml')
                et = etree.XML(commentsXML)
                comments = et.xpath('//w:comment', namespaces=ooXMLns)
            except zipfile.BadZipFile:
                self._result_log_list.append('Невозможно прочитать данные из пояснительной записки.'
                                             ' Ошибка доступа к файлу')
                logging.error('Failed to get text in %s', self._name)
            except gitlab.GitlabGetError:
                self._result_log_list.append('Невозможно получить ПЗ с ресурса Gitlab')
                logging.error('Failed to get text from Gitlab in %s', self._name)
            except (KeyError, AttributeError):
                self._result['note_words'] = 0
        return len(comments)

    def _count_note(self):
        result = self._text
        count = 0
        if result in self._project_tree:
            try:
                note_file = self._repository_data.files.get(file_path=result,
                                                            ref='master')
                count = get_metadata(self, note_file)
            except zipfile.BadZipFile:
                self._result_log_list.append('Невозможно прочитать данные из пояснительной записки.'
                                             ' Ошибка доступа к файлу')
                logging.error('Failed to get text in %s', self._name)
            except gitlab.GitlabGetError:
                self._result_log_list.append('Невозможно получить ПЗ с ресурса Gitlab')
                logging.error('Failed to get text from Gitlab in %s', self._name)
        return result in self._project_tree if not self._is_full else count

    def _count_text(self, type_of_work):
        count = 0
        if self._result_docx in self._project_tree:
            try:
                text_file = self._repository_data.files.get(file_path=self._result_docx,
                                                            ref='master')
                count = get_metadata(self, text_file)
            except zipfile.BadZipFile:
                self._result_log_list.append('Невозможно прочитать данные из текста выступления.'
                                             ' Ошибка доступа к файлу')
                logging.error('Failed to get result.docx in %s', self._name)
            except gitlab.GitlabGetError:
                self._result_log_list.append('Невозможно получить текст выступления с Gitlab')
                logging.error('Failed to get result.docx from Gitlab in %s', self._name)
        return self._result_docx in self._project_tree if not self._is_full else count

    def _get_project_tree(self):
        self._project_tree = []
        try:
            for i in self._repository_data.repository_tree():
                self._project_tree.append(i['name'])
        except gitlab.GitlabError:
            self._result_log_list.append('Невозможно получить структуры репозитория с Gitlab')

            logging.error('Failed to get repository tree')
            # logging.error('Failed to get repository tree in %s', self._name)
        except AttributeError:
            pass


class FileParser(Parser):
    """
    Расширение родительского класса для работы с файлами, находящихся на локальной файловой системе
    ...

    Атрибуты:
    file_path : str
        Путь до локальной файловой системы
    """

    def __init__(self, repository_data, type_of_work, short_type, repos_type, file_path):
        """
        Устанавливает необходимые атрибуты для объекта parser

        :param
        file_path : str
            Путь до локальной файловой системы
        """

        super().__init__(repository_data, type_of_work, short_type, repos_type)
        self._name = repository_data
        self.__file_path = file_path
        self._get_project_tree()
        self._name = repository_data

    def _has_link(self):
        result = False
        if 'link.txt' in self._project_tree:
            with open(os.path.join(f'{self.__file_path}', 'link.txt'), 'r') as file:
                result = file.read()
        return result

    def _has_project(self):
        if self._repos_type:
            result = bool(os.listdir(f"{self.__file_path}"))
        else:
            result = 'project' in self._project_tree
        return result

    def _not_load(self):
        return 'do-not-load.txt' in self._project_tree

    def _count_slides(self):
        result = self._result_pptx
        if not self._is_full:
            return result in self._project_tree
        if result not in self._project_tree:
            return 0

        count = 0
        try:
            file_path = self._result_pptx
            with open(os.path.join(f'{self.__file_path}', f'{file_path}'), 'rb') as text_file:
                count = get_slides(self, text_file)
        except zipfile.BadZipFile:
            self._result_log_list.append('Невозможно прочитать данные из презентации.'
                                         ' Ошибка доступа к файлу')
            logging.error('Failed to get result.pptx in %s', self._name)

        # реальных слайдов выдает на 2 больше, если презентация не пуста, поэтому уменьшаем на 2
        if count != 0:
            count -= 2
        return count

    def _count_paper(self):
        result = 0
        try:
            if self._paper in self._project_tree:
                with open(os.path.join(f'{self.__file_path}', f'{self._paper}'), 'rb') as text_file:
                    result = get_metadata(self, text_file)
        except zipfile.BadZipFile:
            self._result_log_list.append('Невозможно прочитать данные из статьи.'
                                         ' Ошибка доступа к файлу')
            logging.error('Failed to get paper in %s', self._name)
        return self._paper in self._project_tree if not self._is_full else result

    def _count_note_words(self):
        result = self._text
        comments = []
        if result in self._project_tree:
            try:
                ooXMLns = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}
                docxZip = zipfile.ZipFile(
                    self._repository_data.files.get(file_path=os.path.join(f'{self.__file_path}', f'{result}'),
                                                    ref='master'))
                commentsXML = docxZip.read('word/comments.xml')
                et = etree.XML(commentsXML)
                comments = et.xpath('//w:comment', namespaces=ooXMLns)
            except zipfile.BadZipFile:
                self._result_log_list.append('Невозможно прочитать данные из пояснительной записки.'
                                             ' Ошибка доступа к файлу')
                logging.error('Failed to get text in %s', self._name)
            except gitlab.GitlabGetError:
                self._result_log_list.append('Невозможно получить ПЗ с ресурса Gitlab')
                logging.error('Failed to get text from Gitlab in %s', self._name)
            except KeyError:
                self._result['note_words'] = 0
            except AttributeError:
                self._result['note_words'] = 0
        return len(comments)

    def _count_note(self):
        result = self._text
        count = 0

        try:
            if result in self._project_tree:
                with open(os.path.join(f'{self.__file_path}', f'{result}'), 'rb') as text_file:
                    count = get_metadata(self, text_file)

                    meta_data = str(get_zip(self, text_file).read(os.path.join('word', 'settings.xml')))[2:-1]
                    notes = len(re.findall(r'footnotePr', meta_data)) / 2 + len(re.findall(r'endnotePr', meta_data)) / 2
                    self._result['note_words'] = int(notes)

        except zipfile.BadZipFile:
            self._result_log_list.append('Невозможно прочитать данные из пояснительной записки.'
                                         ' Ошибка доступа к файлу')
            logging.error('Failed to get text in %s', self._name)
        return result in self._project_tree if not self._is_full else count

    def _count_text(self, type_of_work):
        result = 'result2.docx' if type_of_work == 'kurs2' else 'result.docx'
        count = 0
        try:
            if result in self._project_tree:
                file_path = 'result2.docx' if type_of_work == 'kurs2' else 'result.docx'
                with open(os.path.join(f'{self.__file_path}', f'{file_path}'), 'rb') as text_file:
                    count = get_metadata(self, text_file)
        except zipfile.BadZipFile:
            self._result_log_list.append('Невозможно прочитать данные из текста выступления.'
                                         ' Ошибка доступа к файлу')
            logging.error('Failed to get result.docx in %s', self._name)
        return result in self._project_tree if not self._is_full else count

    def _get_project_tree(self):
        self._project_tree = os.listdir(f"{self.__file_path}/")

    def commits_host(self, slush):
        """
        Считает общее число коммитов в случае анализа с хоста

            return:
                число коммитов
        """
        commit = {'dev': 0, 'work': 0}

        if bool(ls(os.path.join(f'{self.__file_path}', '.git'))):
            cd(f'{self.__file_path}')
            commit['work'] = git('rev-list', '--all', '--count')
            cd(os.path.join('..', '..', f'{Config.FOLDER_WITH_CODE_REPOS}', f'{slush}'))
            commit['dev'] = git('rev-list', '--all', '--count')
            cd('/kidp-dl')
        return commit


def has_readme_host(path_to_slush, slush):
    """
    Проверяет наличие README.md в папках dev и work

        return:
            словарь {'dev':bool, 'work':bool}
    """
    dev_readme = 'README.md' in os.listdir(os.path.join(f'{path_to_slush}',
                                                        f'{Config.FOLDER_WITH_CODE_REPOS}', f'{slush}'))
    work_readme = 'README.md' in os.listdir(os.path.join(f'{path_to_slush}',
                                                         f'{Config.FOLDER_WITH_DOCK_REPOS}', f'{slush}'))
    readme = {'dev': dev_readme, 'work': work_readme}
    return readme


def get_zip(class_type, file_data):
    """
    Получить декодированные данные

        return:
            zip-файл
    """
    if isinstance(class_type, NetworkParser):
        zip_file = zipfile.ZipFile(io.BytesIO(file_data.decode()))
    else:
        zip_file = zipfile.ZipFile(file_data)
    return zip_file


def get_slides(class_type, file_data):
    """
    Считает и возвращает количество слайдов в презентации

        return:
            Количество слайдов
    """
    meta_data = str(get_zip(class_type, file_data).read(os.path.join('ppt', 'presentation.xml')))
    return len(re.findall(r'p:sldId', meta_data))


def get_metadata(class_type, file_data):
    """
    Обрабатывает метаданные и возвращает число страниц в документе
    :param class_type:
        NetworkParser или FileParser
    :param file_data:
        Метаданные файла
    :return:
        Количество страниц
    """
    meta_data = str(get_zip(class_type, file_data).read(os.path.join('docProps', 'app.xml')))[2:-1]
    return re.findall(r'\d+', re.findall(r'<Pages>\d+</Pages>', meta_data)[0])[0]
