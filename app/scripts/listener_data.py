"""
Файл с классами-обработчиками данных, которые находятся на ресурсе Gitlab
"""
import datetime
import logging
import os
from abc import ABC, abstractmethod


class RepositoryData(ABC):
    """
    Родительский класс
    ...
    Атрибуты:
    repository_data : gitlab.project.Gitlab
        Метаданные репозитория

    Методы:
    get_repository_data()
        Возвращает метаданные репозитория
    get_name()
        Возвращает имя обрабатываемого репозитория
    get_result()
        Возвращает обработанные данные в виде словаря
    """
    _result = {}

    def __init__(self, repository_data):
        """
        Устанавливает необходимый атрибут для объекта repository_data
        :param
        repository_data : gitlab.project.Gitlab
        """
        self._repository_data = repository_data

    def get_repository_data(self):
        """
        Получить метаданные репозитория

        :return:
            Метаданные репозитория
        """
        return self._repository_data

    def get_name(self):
        """
        Получить название репозитория

        :return
            Название репозитория
        """
        return self._repository_data.name

    def get_result(self):
        """
        Получить результат обработки репозитория

        :return
            Словарь, в котором записаны обработанные данные
        """
        return self._result

    @abstractmethod
    def get_data(self):
        """abst_met"""
        pass


class ListenerData(RepositoryData):
    """
    Класс-обработчик одного репозитория
    ...

    Атрибуты:
    repository_data : gitlab
        Метаданные репозитория
    work : str
        Работа, которая содержится в репозитории

    Методы:
    get_data()
        Собирает в словарь данные, необходимые для обработки
    """

    def __init__(self, repository_data, work):
        """
        Устанавливает необходимые атрибуты для объекта listener_data

        :param
        repository_data
            Проброс метаданных репозитория в родительский класс
        :param
        work
            Работа, которая содержится в репозитории
        """
        super().__init__(repository_data)
        self.__work = bool(work == 'kurs2')
        self.get_data()

    def __get_all_issue(self):
        return len(self._repository_data.issues.list(all=True, state='all')) \
               - self.__get_all_issue_milestone()

    def __get_closed_issue(self):
        return len(self._repository_data.issues.list(all=True, state='closed')) \
               - self.__get_closed_issue_milestone()

    def __get_commit_count(self):
        return len(self._repository_data.commits.list(all=True))

    def __get_closed_issue_milestone(self):
        mile_list = self._repository_data.milestones.list(all=True, state='all')
        if len(mile_list) == 0:
            return 0
        else:
            curr_mile = mile_list[0]
            if self.__work:
                for i in mile_list:
                    if i.title.split()[0] == 'курсовая2':
                        curr_mile = i
            else:
                for i in mile_list:
                    work = i.title.split()[0]
                    if work in ['курсовая', 'диплом', 'практика']:
                        curr_mile = i
            return len(self._repository_data.issues.list(milestone=curr_mile.title, state='closed'))

    def __get_all_issue_milestone(self):
        mile_list = self._repository_data.milestones.list(all=True, state='all')
        if len(mile_list) == 0:
            result = 0
        else:
            curr_mile = mile_list[0]
            if self.__work:
                for i in mile_list:
                    if i.title.split()[0] == 'курсовая2':
                        curr_mile = i
            else:
                for i in mile_list:
                    work = i.title.split()[0]
                    if work in ['курсовая', 'диплом', 'практика']:
                        curr_mile = i
            result = len(self._repository_data.issues.list(milestone=curr_mile.title, state='all'))
        return result

    def __get_size(self):
        try:
            result = self._repository_data.statistics["repository_size"]
        except AttributeError as stats_error:
            logging.error('Failed to print %s', stats_error)
            result = -1
        return result

    def get_data(self):
        """
        Заполняет словарь обработанными данными
        """
        self._result.clear()
        self._result['full_name'] = self.get_name()
        self._result['closed_issue'] = self.__get_closed_issue()
        self._result['all_issue'] = self.__get_all_issue()
        self._result['closed_issue_ms'] = self.__get_closed_issue_milestone()
        self._result['all_issue_ms'] = self.__get_all_issue_milestone()
        self._result['size'] = self.__get_size()


class MergeRequestData(RepositoryData):
    """
    Класс-обработчик Merge Request-ов

    Атрибуты:
    repository_data : gitlab.object
        Метаданные репозитория

    Методы:
    get_data()
        Собирает в словарь данные, необходимые для обработки
    get_result()

    get_stats()
    """
    __result_list = []

    def get_stats(self):
        """
        Обрабатывает каждый Merge Request

        :return:
        Список со словарями. В каждом словаре обработанные данные по каждому MR
        """
        self.__result_list.clear()
        for i in self._repository_data.mergerequests.list(state='opened'):
            self.get_data()
            # mr_data = MergeRequest(i)
            self._result.update(MergeRequest(i).get_data())
            self.__result_list.append(self._result.copy())
        return self.__result_list

    def get_result(self):
        """
        Получить список словарей

        :return:
            Список словарей
        """
        return self.__result_list

    def get_data(self):
        """
        Заполняет словарь названием репозитория и датой обработки
        """
        self._result.clear()
        self._result['project'] = self._repository_data.name
        self._result['data'] = datetime.date.today().strftime("%d.%m.%Y")


class MergeRequest:
    """
    Класс-обработчик конкретного Merge Request

    Атрибуты:
    merge_request
        Данные одного Merge Request
    """

    def __init__(self, merge_request):
        """
        Устанавливает необходимый атрибут для объекта merge_request

        :param
        merge_request
            Данные одного Merge Request
        """
        self.__mr_result = {}
        self.merge_request = merge_request

    def get_data(self):
        """
        Собирает в словарь обработанные данные

        :return:
        Словарь с данными по одному Merge request
        """
        self.__mr_result['full_name'] = self.__get_author()
        self.__mr_result['mr_name'] = self.__get_name()
        self.__mr_result['unresolved'] = os.path.join(f"{self.__get_closed_threads()}", f"{self.__get_all_threads()}")
        self.__mr_result['overviews'] = self.__get_overviews()
        self.__mr_result['commits'] = self.__get_commit_count()
        return self.__mr_result

    def get_result(self):
        """
        Получить обработанные данные

        :return:
        Словарь с данными по одному Merge request
        """
        return self.__mr_result

    def __get_author(self):
        return self.merge_request.author['name']

    def __get_name(self):
        return self.merge_request.title

    def __get_commit_count(self):
        return len(self.merge_request.commits())

    def __get_closed_threads(self):
        closed_threads = 0
        for i in self.merge_request.discussions.list():
            if i.attributes['notes'][0]['resolvable']:
                if i.attributes['notes'][0]['resolved']:
                    closed_threads += 1
        return closed_threads

    def __get_all_threads(self):
        all_threads = 0
        closed_threads = 0
        for i in self.merge_request.discussions.list():
            if i.attributes['notes'][0]['resolvable']:
                all_threads += 1
                if i.attributes['notes'][0]['resolved']:
                    closed_threads += 1
        return all_threads + closed_threads

    def __get_overviews(self):
        return self.merge_request.user_notes_count
