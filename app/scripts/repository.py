"""
Файл с классами, необходимыми для получения репозиториев
"""
import logging
import requests
import os
from config import Config


class Repository:
    """
    Класс для поиска репозиториев по имени и возвращения списка id
    ...

    Атрибуты:
    repos_name : str
        путь с списку репозиториев
    g_lab : gitlab.client.Gitlab
        объект, полученный из класса Connection

    Методы:
    get_id_list()
        Получить список с id необходимых для обработки репозиториев
    get_repository(repos_id)
        Получить метаданные репозитория по его id
    """
    _id_list = []

    def __init__(self, repos_name, g_lab):
        """
        Устанавливает необходимые атрибуты для объекта repository

        :param
        repos_name : str
            путь с списку репозиториев
        :param
        g_lab : gitlab.client.Gitlab
            объект, полученный из класса Connection
        """
        self._repos_name = repos_name
        self._g_lab = g_lab

    def get_id_list(self):
        """
        Получает список репозиториев, находящихся по указанному пути

        :return
            Список с id найденных репозиториев при условии, что путь существует
                и содержит проекты
            Пустой список в противном случае
        """
        self._id_list.clear()

        try:
            g_lab_list = self._g_lab.groups.list(all=True,
                                                 search=os.path.join(f"{Config.FOLDER_WITH_REPOS}",
                                                                     f"{self._repos_name}"),
                                                 order_by='name',
                                                 sort='asc',
                                                 per_page=40)

            for item in g_lab_list:

                for i in self._g_lab.groups.get(item.get_id()).projects.list():
                    self._id_list.append(i.id)
        except requests.exceptions.ConnectionError:
            logging.error('Failed to get repository list, repository not exist or bad name')
        except IndexError:
            logging.error('Failed to get id list, repository bad name')
        return self._id_list

    def get_repository(self, repos_id):
        """
        Получает метаданные конкретного репозитория по его id

        :arg
        repos_id : int
            id репозитория Gitlab

        :return
            Метаданные указанного репозитория
        """
        return self._g_lab.projects.get(repos_id, statistics=1)


class MergeRequestRepository(Repository):
    """
    Расширение класса Repository для работы с одним репозиторием

    Методы:
    get_id_list()
        Получить id репозитория по его имени
    """

    def get_id_list(self):
        """
        Получает репозиторий, находящихся по указанному пути

        :return
            id найденного репозитория при условии, что путь существует
            False в противном случае
        """
        result = False
        try:
            result = self._g_lab.projects.get(self._repos_name).id
        except requests.exceptions.ConnectionError:
            logging.error('Failed to get repository list, repository not exist or bad name')
        except IndexError:
            logging.error('Failed to get id list, repository bad name')
        return result
