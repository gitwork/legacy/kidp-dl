"""
Файл, связывающий веб и результаты обработки репозиториев
"""
import os
import pathlib
import gitlab
from app.scripts.repository import Repository
from flask import Flask, url_for, redirect
from flask import request, render_template, send_file
from flask_bootstrap import Bootstrap
from config import Config
from app.scripts.valid_values import ValidValues
from app.scripts.kidp_results import PrintRepository, PrintMergeRequest
from app.scripts.repository import Repository
from app.scripts.result import Result, RepositoryResult, MergeRequestResult
from app.scripts.gitlab_connection import Connection
import logging

app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config.from_object(Config)

dict_with_input_items = {}
list_with_input_items = []
REPOS_PATH = os.path.join('/kidp-dl', 'app', 'tmp', 'results/')
data_list = []


@app.route('/', methods=['GET', 'POST'])
def repo_stats():
    """
    Получает данные из форм по КИДП

    :return
    Страница с таблицей
    """
    if request.method == 'POST':
        dict_with_input_items['reposName'] = request.form['reposName']
        dict_with_input_items['radioH'] = request.form['radioH']
        dict_with_input_items['inputToken'] = request.form['inputToken']
        dict_with_input_items['radio'] = request.form['radio']
        dict_with_input_items['radioS'] = request.form['radioS']
        dict_with_input_items['radioR'] = request.form['radioR']
        if ValidValues(dict_with_input_items).check_repository():
            return redirect(url_for('table'))
    return render_template('main.html')


@app.route('/stats', methods=['GET', 'POST'])
def stats():
    """
    Получает данные из форм по Merge Request

    :return
    Страница с таблицей
    """
    if request.method == 'POST':
        for i in request.form.keys():
            dict_with_input_items[i] = request.form[i]
        if ValidValues(dict_with_input_items).check_mr():
            return redirect(url_for('mr_table'))
    return render_template('stats.html')


def pagination(git_connect, g_lab_list, names_list):
    """
    Заполняет список имен репозиториев для последующей выкачки
    """
    projects_on_page = []
    page = 0
    while True:
        try:
            for item in g_lab_list:
                projects_on_page = git_connect.groups.get(item.get_id()).projects.list(per_page=20, page=page)
                for project in projects_on_page:
                    names_list.append(project.name)
        except gitlab.exceptions.GitlabListError as err:
            logging.error(f"Cant get project list: {err}")
        if not projects_on_page:
            break
        page += 1


@app.route('/local', methods=['GET', 'POST'])
def local():
    """
    Позволяет склонировать репозитории из группы из списка с fullname
    в опеределенную папку

    :return
    Файл с отчетом
    """
    if request.method == 'POST':
        for key in request.form.keys():
            list_with_input_items.append(request.form[key])

        git_connect = Connection(list_with_input_items[2]).get_connection()
        path_folder = list_with_input_items[0].split('/')[2] + '/' + list_with_input_items[0].split('/')[3]
        g_lab_list_dev = git_connect.groups.list(all=True,
                                                 search=os.path.join(f"{Config.FOLDER_WITH_REPOS}", f"{path_folder}",
                                                                     f"{Config.FOLDER_WITH_CODE_REPOS}"),
                                                 order_by='name',
                                                 sort='asc',
                                                 per_page=40)
        g_lab_list_work = git_connect.groups.list(all=True,
                                                  search=os.path.join(f"{Config.FOLDER_WITH_REPOS}", f"{path_folder}",
                                                                      f"{Config.FOLDER_WITH_DOCK_REPOS}"),
                                                  order_by='name',
                                                  sort='asc',
                                                  per_page=40)
        names_list_dev = []
        names_list_work = []
        pagination(git_connect, g_lab_list_dev, names_list_dev)
        pagination(git_connect, g_lab_list_work, names_list_work)
        list_of_names = list(set(names_list_work + names_list_dev))

        path = pathlib.Path(os.path.join(f"{REPOS_PATH}", f"{path_folder}", f"{Config.FOLDER_WITH_DOCK_REPOS}"))
        path.mkdir(parents=True, exist_ok=True)
        path = pathlib.Path(os.path.join(f"{REPOS_PATH}", f"{path_folder}", f"{Config.FOLDER_WITH_CODE_REPOS}"))
        path.mkdir(parents=True, exist_ok=True)
        for name in list_of_names:
            logging.basicConfig(level=logging.DEBUG)

            # ниже работа с dev-репо
            os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_CODE_REPOS}'))
            # если репо уже скачан - обновляем
            if os.path.exists(f"{name}"):
                logging.info(f"Pulling {Config.FOLDER_WITH_CODE_REPOS}/{name}")
                os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_CODE_REPOS}', f'{name}'))
                os.system(f"git -c http.sslVerify=false pull &> /dev/null")
            # в противном случае выкачиваем
            else:
                logging.info(f"Cloning {Config.FOLDER_WITH_CODE_REPOS}/{name} "
                             f"into {REPOS_PATH}{path_folder}/{Config.FOLDER_WITH_CODE_REPOS}")

                os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_CODE_REPOS}'))
                os.system(f"git -c http.sslVerify=false clone "
                          f"https://{list_with_input_items[1]}:{list_with_input_items[2]}@{list_with_input_items[0]}"
                          f"/{Config.FOLDER_WITH_CODE_REPOS}/{name}.git &> /dev/null")
            # если не удалось скачать
            os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_CODE_REPOS}'))
            if not os.path.exists(f"{name}"):
                logging.info(f"Error of cloning {Config.FOLDER_WITH_CODE_REPOS}/{name}")

            # ниже работа с work-репо
            os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_DOCK_REPOS}'))
            # если репо уже скачан - обновляем
            if os.path.exists(f"{name}"):
                logging.info(f"Pulling {Config.FOLDER_WITH_DOCK_REPOS}/{name}")
                os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_DOCK_REPOS}', f'{name}'))
                os.system(f"git -c http.sslVerify=false pull &> /dev/null")
            # в противном случае выкачиваем
            else:
                logging.info(f"Cloning {Config.FOLDER_WITH_DOCK_REPOS}/{name} "
                             f"into {REPOS_PATH}{path_folder}/{Config.FOLDER_WITH_DOCK_REPOS}")

                os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_DOCK_REPOS}'))
                os.system(f"git -c http.sslVerify=false clone "
                          f"https://{list_with_input_items[1]}:{list_with_input_items[2]}@{list_with_input_items[0]}"
                          f"/{Config.FOLDER_WITH_DOCK_REPOS}/{name}.git &> /dev/null")
            # если не удалось скачать
            os.chdir(os.path.join(f'{REPOS_PATH}', f'{path_folder}', f'{Config.FOLDER_WITH_DOCK_REPOS}'))
            if not os.path.exists(f"{name}"):
                logging.info(f"Error of cloning {Config.FOLDER_WITH_DOCK_REPOS}/{name}")

        os.chdir("/kidp-dl")
        return render_template('main.html')
    return render_template('local.html')


@app.route('/about')
def about():
    """
    :return
    Страница со справочной информацией
    """
    return render_template('about.html')


@app.route('/mr_table')
def mr_table():
    """
    Обрабатывает данные по Merge Request

    :return
    Веб-страница с таблицей
    """
    kidp = PrintMergeRequest(MergeRequestResult(ValidValues(dict_with_input_items).check_list(),
                                                dict_with_input_items.get('inputToken')).get_stats(), {},
                             'net', dict_with_input_items.get('reposName'))
    data = kidp.web_table()
    header = kidp.get_header()
    return render_template('mr_table.html', data=data, header=header)


@app.route('/table')
def table():
    """
    Обрабатывает данные по КИДП

    :return
    Веб-страница с таблицей
    """
    value_list = []
    result = RepositoryResult(dict_with_input_items.get('reposName'), dict_with_input_items.get('inputToken'),
                              dict_with_input_items.get('radio'),
                              dict_with_input_items.get('radioS'), dict_with_input_items.get('radioH'),
                              dict_with_input_items.get('radioR'))
    if dict_with_input_items.get('radioH') == 'net':
        kidp = PrintRepository(result.get_stats(), result.get_log_list(),
                               dict_with_input_items.get('radioH'), dict_with_input_items.get('reposName'))
        res_web_table = kidp.web_table()
        data = res_web_table[0]
        branches = res_web_table[1]
    else:
        kidp = PrintRepository(result.get_stats_host(), result.get_log_list(),
                               dict_with_input_items.get('radioH'), dict_with_input_items.get('reposName'))
        res_web_table = kidp.web_table()
        data = res_web_table

    # info почему-то не логгируется, пишу в error
    logging.error(data)
    # в любом элементе data 0 элемент - fullname, 1 - список для dev [число-веток, ссылка-на-ветки]
    # в эксельку нужно выгружать только число веток без ссылок
    if dict_with_input_items.get('radioH') == 'net':
        for slush in range(len(data)):
            data[slush][1] = branches[slush][0]
            data[slush][2] = branches[slush][1]

    logging.error(data)
    header = kidp.get_header()
    log = kidp.make_log()

    if dict_with_input_items.get('radioH') == 'net':
        for i in data:
            value_list.append(i[1])
        return render_template('table.html', data=data, header=header,
                               values=value_list, log=log,
                               branch=branches)
    return render_template('table.html', data=data, header=header,
                           values=value_list, log=log)


@app.route('/download', methods=['POST'])
def some_sum():
    """
    :return
    Окно загрузки файла формата csv
    """
    return send_file(os.path.join(".", "tmp", "result.csv"))
